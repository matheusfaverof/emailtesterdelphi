object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Teste de Servi'#231'o'
  ClientHeight = 569
  ClientWidth = 646
  Color = clBtnHighlight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Position = poDesigned
  TextHeight = 15
  object LabelTitulo: TLabel
    Left = 168
    Top = 8
    Width = 292
    Height = 21
    Caption = 'Ferramenta de Testes para Envio de E-mail'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Button_Ping: TButton
    Left = 17
    Top = 532
    Width = 113
    Height = 33
    Caption = 'Ping'
    TabOrder = 0
    OnClick = Button_PingClick
  end
  object GrupoTeste: TGroupBox
    Left = 16
    Top = 334
    Width = 616
    Height = 192
    Caption = 'Par'#226'metros do Teste'
    TabOrder = 1
    object EmailDestinatario: TLabeledEdit
      Left = 48
      Top = 50
      Width = 289
      Height = 23
      EditLabel.Width = 282
      EditLabel.Height = 15
      EditLabel.Caption = 'Endere'#231'o de E-mail do Destinatario (Destino do Teste)'
      TabOrder = 0
      Text = ''
    end
    object EmailCorpo: TMemo
      Left = 48
      Top = 92
      Width = 289
      Height = 89
      Lines.Strings = (
        'EmailCorpo')
      TabOrder = 1
      StyleName = 'Corpo do Email'
    end
  end
  object ClienteGrupo: TGroupBox
    Left = 16
    Top = 42
    Width = 616
    Height = 286
    Caption = 'Dados de Email do Cliente'
    TabOrder = 2
    object EmailRemetente: TLabeledEdit
      Left = 48
      Top = 218
      Width = 289
      Height = 23
      EditLabel.Width = 179
      EditLabel.Height = 15
      EditLabel.Caption = 'Endere'#231'o de E-mail do Remetente'
      TabOrder = 0
      Text = ''
    end
    object Senha: TLabeledEdit
      Left = 48
      Top = 168
      Width = 289
      Height = 23
      EditLabel.Width = 32
      EditLabel.Height = 15
      EditLabel.Caption = 'Senha'
      TabOrder = 1
      Text = ''
    end
    object ServerPath: TLabeledEdit
      Left = 48
      Top = 72
      Width = 289
      Height = 23
      EditLabel.Width = 112
      EditLabel.Height = 15
      EditLabel.Caption = 'Endere'#231'o do Servidor'
      TabOrder = 2
      Text = ''
    end
    object ServerPort: TLabeledEdit
      Left = 360
      Top = 72
      Width = 73
      Height = 23
      EditLabel.Width = 70
      EditLabel.Height = 15
      EditLabel.Caption = 'Porta (P: 587)'
      TabOrder = 3
      Text = ''
    end
    object Usuario: TLabeledEdit
      Left = 48
      Top = 120
      Width = 289
      Height = 23
      EditLabel.Width = 40
      EditLabel.Height = 15
      EditLabel.Caption = 'Usuario'
      TabOrder = 4
      Text = ''
    end
    object EmailSSL: TCheckBox
      Left = 363
      Top = 124
      Width = 97
      Height = 15
      Caption = 'Utiliza SSL'
      TabOrder = 5
    end
  end
  object Button_EnviarEmail: TButton
    Left = 519
    Top = 532
    Width = 113
    Height = 33
    Caption = 'Enviar E-mail'
    TabOrder = 3
    OnClick = Button_EnviarEmailClick
  end
  object Button_Telnet: TButton
    Left = 151
    Top = 532
    Width = 113
    Height = 33
    Caption = 'Telnet'
    TabOrder = 4
    OnClick = Button_TelnetClick
  end
  object IdIcmpClient1: TIdIcmpClient
    Protocol = 1
    ProtocolIPv6 = 58
    Left = 576
    Top = 358
  end
  object IdTelnet1: TIdTelnet
    Terminal = 'dumb'
    Left = 576
    Top = 422
  end
  object IdSMTP1: TIdSMTP
    SASLMechanisms = <>
    Left = 512
    Top = 430
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    MaxLineAction = maException
    Port = 0
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 504
    Top = 366
  end
  object IdMessage1: TIdMessage
    AttachmentEncoding = 'UUE'
    BccList = <>
    CCList = <>
    Encoding = meDefault
    FromList = <
      item
      end>
    Recipients = <>
    ReplyTo = <>
    ConvertPreamble = True
    Left = 448
    Top = 446
  end
end
