unit EmailTester;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  Datasnap.DBClient, Datasnap.Win.MConnect, Datasnap.Win.SConnect,
  IdBaseComponent, IdComponent, IdRawBase, IdRawClient, IdIcmpClient,
  IdTCPConnection, IdTCPClient, IdTelnet, IdMailBox, IdMessage,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP,
  IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL;

type
  TForm2 = class(TForm)
    Button_Ping: TButton;
    LabelTitulo: TLabel;
    ServerPath: TLabeledEdit;
    ServerPort: TLabeledEdit;
    Usuario: TLabeledEdit;
    Senha: TLabeledEdit;
    EmailRemetente: TLabeledEdit;
    EmailDestinatario: TLabeledEdit;
    GrupoTeste: TGroupBox;
    ClienteGrupo: TGroupBox;
    EmailCorpo: TMemo;
    EmailSSL: TCheckBox;
    Button_EnviarEmail: TButton;
    IdIcmpClient1: TIdIcmpClient;
    IdTelnet1: TIdTelnet;
    Button_Telnet: TButton;
    IdSMTP1: TIdSMTP;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdMessage1: TIdMessage;
    procedure Button_PingClick(Sender: TObject);
    procedure Button_TelnetClick(Sender: TObject);
    procedure Button_EnviarEmailClick(Sender: TObject);

  private
    { Private declarations }
  public
    function Ping(IP: String): boolean;
    function Telnet(IP: String; Porta: Word): boolean;
    function EnviaEmail(IP: String; Porta: Word; UsaSSL: boolean; Usuario: String; Senha: String; Remetente: String; Destinatario: String; Corpo: String): boolean;
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

//Boto Testar Conexo com Telnet
procedure TForm2.Button_TelnetClick(Sender: TObject);
begin

  //Variaveis
  var IP    := ServerPath.Text;
  var Porta := ServerPort.Text;


  //Valida o preenchimento das Variaveis
  if IP = '' then
    IP := '172.0.0.1';
  if Porta = '' then
    Porta := '587';

  // Realiza um Ping e verifica o Status da Conexo
  if Telnet(IP, StrToInt(Porta)) = false then
    Application.MessageBox('Sem Conexo com o Host de E-maill. O Destino est inalcansavel','Teste de Conexo')
  else
    Application.MessageBox('Conexao com o Host de Email realizada com Sucesso. Nao foi identificado Problemas na Conexo com este endereço!','Teste de Conexo')
end;


procedure TForm2.Button_EnviarEmailClick(Sender: TObject);


begin
  EnviaEmail(
    ServerPath.Text,
    StrToInt(ServerPort.Text),
    True,
    Usuario.Text,
    Senha.Text,
    EmailRemetente.Text,
    EmailDestinatario.Text,
    'Email de Teste');
end;


//Boto Testar Conexo com Ping
procedure TForm2.Button_PingClick(Sender: TObject);
begin
  //Variaveis
  var IP := ServerPath.Text;

  //Valida o Valor da Variavel
  if IP = '' then
    IP := '172.0.0.1';

  if Ping(IP) = False then
    Application.MessageBox('Sem Conexo com o Host de E-maill. O Destino est inalcansavel','Teste de Conexo')
  else
    Application.MessageBox('Conexo com o Host de Email realizada com Sucesso. No foi identificado Problemas na Conexo!','Teste de Conexo')
end;



//Funo de Envio de Email
function TForm2.EnviaEmail(IP: String; Porta: Word; UsaSSL: boolean; Usuario: String; Senha: String; Remetente: String; Destinatario: String; Corpo: String): boolean;
var
  IdSMTP : TIdSMTP;
  IdSSL : TIdSSLIOHandlerSocketOpenSSL;
  IdMessage : TIdMessage;
begin

  IdSSL := TIdSSLIOHandlerSocketOpenSSL.Create();
  IdSMTP := TIdSMTP.Create();
  IdMessage := TIdMessage.Create();
  try
    //Configuraes do SSL
    //IdSSL.SSLOptions.Method := sslvTLSv1_2;
    //IdSSL.SSLOptions.Mode   := sslmClient;
    IdSSl.sslOptions.SSLVersions := [sslvTLSv1, sslvTLSv1_1, sslvTLSv1_2];
    IdSSL.PassThrough := False;

    //Configuraes do SMTP
    IdSMTP.IOHandler := IdSSl;
    IdSMTP.UseTLS := utUseRequireTLS;
    IdSMTP.AuthType := satDefault;

    //Configuraes do Servidor de Email
    IdSMTP.Host     := IP;
    IdSMTP.Port     := Porta;
    IdSMTP.Username := Usuario;
    IdSMTP.Password := Senha;

  except
    on e: Exception do
      ShowMessage('Erro ao Instanciar o Servio de Email:' + e.Message);
  end;

  try
    //Configurao da mensagem
    IdMessage.From.Address := Remetente;
    IdMessage.From.Name    := 'Ferramenta de Envio';
    IdMessage.Subject      := 'Teste de Envio - Trafegus';
    IdMessage.Recipients.Add.Text := Destinatario;
    IdMessage.Encoding     := meMIME;
    //IdMessage.Body := '';

  except
    on e: Exception do
      ShowMessage('Erro ao Configurar os dados para Envio do Email' + e.Message);
  end;

  try
    //Conexo
    try
      IdSMTP.Connect;
      IdSMTP.Authenticate;
    except
      on e: Exception do
        ShowMessage('Ocorreu um erro ao Realizar a Autenticao no Servidor' + e.Message);
    end;

    //Enviar o Email
    try
      IdSMTP.Send(IdMessage);
      ShowMessage('Email Enviado!');
    except
      on e: Exception do
        ShowMessage('Ocorreu um erro ao Enviar o Email ' + e.Message);
    end;

  finally
    //Desconecta do Servidor
    IdSMTP.Disconnect;
    UnLoadOpenSSLLibrary;
  end;
end;

//Realiza um Telnet no IP e Porta do Servidor de Email
function TForm2.Telnet(IP: String; Porta: Word): boolean;
var
  Telnet : TIdTelnet;
  ABuffer: String;
begin
  Telnet := TIdTelnet.Create(nil);
  try
     try
        Telnet.Host := IP;
        Telnet.Port := Porta;
        Telnet.ConnectTimeout := 10000;
        Telnet.Connect;

      if Telnet.Connected then
        Result := True
      else
        Result := False;
     except
       Result := False;
     end;
  finally
    Telnet.Destroy;
  end;
end;

// Realiza um Ping no Host do Servidor de Email e Retorna True ou False
function TForm2.Ping(IP: String): boolean;
var
  ICMP : TIdIcmpClient;
  ABuffer: String;
begin
    ICMP := TIdIcmpClient.Create(nil);
    try
        try
          ICMP.PacketSize := 32;
          ICMP.Host := IP;
          ICMP.ReceiveTimeout := 10000;
          ICMP.Protocol := 1;
          ABuffer := ICMP.Host + StringOfChar(' ', 255);
          ICMP.Ping(ABuffer); //Envia o Pacote para o Ping

          if ICMP.ReplyStatus.BytesReceived > 0 then
            Result := True
          else
            Result := False;
        except
          Result := False;
        end;
    finally
       ICMP.Destroy;
    end;
end;

end.
